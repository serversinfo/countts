#include <sourcemod>
#include <sdktools>
//#include <countts>
#include <sm_jail_redie>
#include <devzones>
#include <shop_tpos>
#include <csgo_colors>
#define VERSION "1.1.76"

public Plugin myinfo =
{
	name			= "Check T Count",
	author			= "ShaRen",
	description		= "Помогает КТ быстро посчитать заключенных",
	version			= VERSION
}

//#define DEBUG
//#if defined DEBUG
//stock void debugMessage(const char[] message, any ...)
//{
//	char szMessage[256], szPath[PLATFORM_MAX_PATH];
//	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_countts.txt");
//	VFormat(szMessage, sizeof(szMessage), message, 2);
//	LogToFile(szPath, szMessage);
//}
//#define dbgMsg(%0) debugMessage(%0)
//#elseв
//#define dbgMsg(%0)
//#endif

Handle g_everyone_is_here 			= null;	// когда все собраны первый раз
Handle g_everyone_is_here_every 	= null;	// когда все собраны каждый раз
Handle g_countts_check 				= null;	// когда вводят !check
Handle h_Timer_DisableTPos			= null;	// отключает функцию tpos если Т давно не пересчитывали

bool g_bRunner[MAXPLAYERS+1];		// Сбежал ли Т?
bool g_bGames;						// Доступны ли игры?
bool g_bTpos;						// Доступен ли tpos?
bool g_bAll_t;						// Собрали ли всех зеков?
float g_fFloodTime;					// для антифлуда
int g_iCount;						// нужно чтобы антифлуд сбрасывался при изменении Т

public void OnPluginStart()
{
	RegConsoleCmd("sm_check", Check);
	HookEvent("round_start", round_start, EventHookMode_PostNoCopy);
	g_everyone_is_here		 = CreateGlobalForward("everyone_is_here", ET_Ignore, Param_Cell);
	g_everyone_is_here_every = CreateGlobalForward("everyone_is_here_every", ET_Ignore, Param_Cell);
	g_countts_check			 = CreateGlobalForward("countts_check", ET_Ignore, Param_Cell);
	//dbgMsg("public void OnPluginStart()-----------------------");
	CreateTimer(4.0, TimerCheck, TIMER_REPEAT);
}

public Action TimerCheck(Handle:timer)
{
	if (!g_bAll_t)
		Check(0,0);
}

public Action round_start(Handle event, char[] name, bool silent)
{
	g_bGames = false;
	g_bAll_t = false;
	
	for (int i=1; i<=MaxClients; i++)
		g_bRunner[i] = false;
	//dbgMsg("public Action round_start");
}

public KillTimerTPos()
{
	if (h_Timer_DisableTPos != null) {
		KillTimer(h_Timer_DisableTPos);
		h_Timer_DisableTPos = null;
	}
	h_Timer_DisableTPos = CreateTimer(35.0, Timer_DisableTPos);
	g_bTpos = true;
}

public Action Timer_DisableTPos(Handle:timer)
{
	h_Timer_DisableTPos = null;
	g_bTpos = false;
	for (int i=1; i<=MaxClients; i++)
		if ( IsPlayerValid(i) && GetClientTeam(i) == 3) {
			PrintToChat(i, "!tpos показывает только сбежавших, поэтому перед использованием");
			PrintToChat(i, "нужно в ручную обновлять информацию о сбежавших командой !check");
		}
}

/*Глобальные ф-ции (Для остальных плагинов)*/
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("IsTRunner",		Native_IsTRunner);
	CreateNative("GamesAvailable",	Native_GamesAvailable);
	CreateNative("TPosAvailable",	Native_TPosAvailable);
	RegPluginLibrary("countts");
	return APLRes_Success;
}

public Native_IsTRunner(Handle plugin, int numParams)
{
	//int client = GetNativeCell(1);
	
	//if(!IsClientInGame(client) && !IsClientConnected(client))
	//	ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", client);
	
	return(g_bRunner[GetNativeCell(1)]) ? true:false;
}

public Native_GamesAvailable(Handle:plugin, numParams)
{
	return(g_bGames) ? true:false;
}

public Native_TPosAvailable(Handle:plugin, numParams)
{
	return(g_bTpos)? true:false;
}

public void OnClientDisconnect(int client)
{
	g_bRunner[client] = false;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if(!IsClientInGame(client) || IsFakeClient(client))
		return Plugin_Continue;

	static iButtons[MAXPLAYERS+1] = {0, ...};

	if((buttons & IN_USE) && !(iButtons[client] & IN_USE)) {
		int iTarget = GetClientAimTarget2(client, true);		
		if(0<iTarget<65 && IsClientInGame(iTarget) && IsPlayerAlive(iTarget) && !IsPlayerGhost(iTarget)) 
			if(GetClientTeam(client) == 3 && IsPlayerAlive(client) && !IsPlayerGhost(client))
				if (IsPlayerValid(iTarget) && GetClientTeam(iTarget) == 2) {
					float fPos[MAXPLAYERS+1][3];

					int iAllts;
					for (int i=1; i<=MaxClients; i++)
						if (IsPlayerValid(i) && GetClientTeam(i) == 2) {
							GetClientAbsOrigin(i, fPos[i]);
							iAllts++;
						}

					bool bNear[MAXPLAYERS+1][MAXPLAYERS+1];		// если игроки рядом
					for (int i=MaxClients; i>0; i--)
						if (IsPlayerValid(i) && GetClientTeam(i) == 2)
							for (int j=i-1; j>0; j--)				// считаем в обратку чтобы во втором цикле проходило не от j до 64, а j до 1. В итоге будет меньше циклов
								if (IsPlayerValid(j) && GetClientTeam(j) == 2)
									if (GetVectorDistance(fPos[i], fPos[j]) <= 100.0 ) {
										bNear[i][j] = true;
										bNear[j][i] = true;
										//PrintToConsole(client, "%N(%i) рядом с %N(%i) ", i, i, j, j);
									}

					for (int ii; ii<2; ii++)
						for (int i=MaxClients; i>0; i--)
							if (IsPlayerValid(i) && GetClientTeam(i) == 2)
								for (int j=i-1; j>0; j--)
									if (IsPlayerValid(j) && GetClientTeam(j) == 2)
										if (bNear[i][j] == true)
											for (int z=MaxClients; z>0; z--) {
												if (IsPlayerValid(z) && GetClientTeam(z) == 2 && i != z && j != z)
													if (bNear[i][z] == true) {
														bNear[j][z] = true;
														bNear[z][j] = true;
														//PrintToConsole(client, "%N(%i) рядом с %N(%i) ++++++", j, j, z, z);
													} else if (bNear[j][z] == true) {
														bNear[i][z] = true;
														bNear[z][i] = true;
														//PrintToConsole(client, "%N(%i) рядом с %N(%i) =======", i, i, z, z);
													}
												
													
											}
					int count;
					bool bRunner[MAXPLAYERS+1];
					for (int i=1; i<=MaxClients; i++)
						if (IsPlayerValid(i) && GetClientTeam(i) == 2) {
							if (iTarget ==i) {
								count++;
								bRunner[i] = false;
								continue;
							}
							
							if (bNear[iTarget][i] == true) {
								count++;
								bRunner[i] = false;
								//PrintToConsole(client, "%N(%i) в одной толпе с %N(%i) ", iTarget, iTarget, i, i);
							} else bRunner[i] = true;			// PrintToConsole(client, "%N(%i) ННННЕЕЕЕЕЕЕ в одной толпе с %N(%i) ", iTarget, iTarget, i, i);
						}

					//PrintToConsole(client, "%N в одной толпе с %i зеками", iTarget, count);
					
					//PrintToConsole(client, "count %i ", count);
					if (count == iAllts) {			// если все здесь
						g_bGames = true;
						g_bTpos = false;
						if (client)
							if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != count) {
								g_fFloodTime = GetGameTime();
								g_iCount = count;
								CGOPrintToChatAll("Все {BLUE}%d{DEFAULT} заключенных на месте.", iAllts);
							}
						//dbgMsg("Все %d заключенных на месте.", iAllts);
						Call_StartForward(g_everyone_is_here_every);
						Call_Finish();
						if (!g_bAll_t) {
							Call_StartForward(g_everyone_is_here);
							Call_Finish();
							g_bAll_t = true;
						}
						return Plugin_Continue;
					} else if (count >= 3) {
						for (int i=1; i<MaxClients; i++)
							if (bRunner[i]) {
								g_bRunner[i] = true;
							} else DPos(i);
							
						if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != count) {
							g_fFloodTime = GetGameTime();
							g_iCount = count;
							CGOPrintToChatAll("На месте {BLUE}%d/%d{DEFAULT}, отсутствуют {BLUE}%d", count , iAllts, iAllts-count);

							if (iAllts/count > 2.4 && GetGameTime() - g_fFloodTime > 20.0)
								for (int i=1; i<=MaxClients; i++) 
									if (g_bRunner[i])
										PrintToChat(i, "Если вы хотите сдаться, то напишите !cap, после чего КТ сможет вас телепортировать к себе, при этом вас не убьют за прошлые бунты");
						}
						g_bTpos = true;
						PrintToChat(client, "Чтобы найти %d сбежавших используй !tpos", iAllts-count);
					}
				}
	}

	iButtons[client] = buttons;
	
	return Plugin_Continue;
}

int GetClientAimTarget2(int client, bool only_clients = true)
{
	float eyeloc[3], ang[3];
	GetClientEyePosition(client, eyeloc);
	GetClientEyeAngles(client, ang);
	TR_TraceRayFilter(eyeloc, ang, MASK_SOLID, RayType_Infinite, TRFilter_AimTarget, client);
	int entity = TR_GetEntityIndex();
	if((only_clients && entity >= 1 && entity <= MaxClients) || (entity > 0) )
		return entity;
	return -1;
}

public bool TRFilter_AimTarget(int entity, int mask, any client)
{
	return (entity == client) ? false:true;
}

public Action Check(int client, int args)
{
	if (client && (!IsPlayerValid(client) || GetClientTeam(client) != 3)) {
		PrintToChat(client, "Только живые КТ могут считать Т");
		return Plugin_Continue;
	}

	float fPos[MAXPLAYERS+1][3];
	int iAllts;
	for (int i=1; i<=MaxClients; i++) {
		g_bRunner[i] = false;
		if (IsPlayerValid(i) && GetClientTeam(i) == 2) {
			GetClientAbsOrigin(i, fPos[i]);
			iAllts++;
		}
	}
	//PrintToChatAll("всего %i зеков ", iAllts);
	if(Zone_CheckIfZoneExists("jail", false)) {
		int iTsInZone;
		for (int i=1; i<=MaxClients; i++)
			if (IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 )
				if (Zone_IsClientInZone(i, "jail", false)) 
					iTsInZone++;
		PrintToServer("iTsInZone %i iAllts %i", iTsInZone, iAllts);
		if (iTsInZone == iAllts) {
			if (client) {
				if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != iTsInZone) {
					g_fFloodTime = GetGameTime();
					g_iCount = iTsInZone;
					CGOPrintToChatAll("Все {BLUE}%d {DEFAULT}заключенных в джайлах.", iAllts);
				} else PrintToChat(client, "Все %d заключенных в джайлах.", iAllts);
			} else PrintToServer("Все %d заключенных в джайлах.", iAllts);
			return Plugin_Continue;
		} else if (iAllts <= iTsInZone*2) {
			if (client) {
				if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != iTsInZone){
					g_fFloodTime = GetGameTime();
					g_iCount = iTsInZone;
					CGOPrintToChatAll("В джайлах {BLUE}%d {DEFAULT}заключенных.", iTsInZone);
				} else PrintToChat(client, "В джайлах %d заключенных.", iTsInZone);
			} else PrintToServer("В джайлах %d заключенных.", iTsInZone);
		}
	} else PrintToServer("jail zone not exist");

	//if(Zone_AllInGame()) {
	//	//g_bGames = true;
	//	//if (client)			PrintToChatAll("Все %d заключенных на месте.", iAllts);
	//	//else PrintToServer("Все %d заключенных на месте.", iAllts);
	//	////dbgMsg("Все %d заключенных на месте.", iAllts);
	//	//Call_StartForward(g_everyone_is_here_every);
	//	//Call_Finish();
	//	//g_bTpos = false;
	//	//if (!g_bAll_t) {
	//	//	Call_StartForward(g_everyone_is_here);
	//	//	Call_Finish();
	//	//	g_bAll_t = true;
	//	//}
	//	return Plugin_Continue;
	//}
	
	bool bNear[MAXPLAYERS+1][MAXPLAYERS+1];		// если игроки рядом
	for (int i=MaxClients; i>0; i--)
		if (IsPlayerValid(i) && GetClientTeam(i) == 2)
			for (int j=i-1; j>0; j--)				// считаем в обратку чтобы во втором цикле проходило не от j до 64, а j до 1. В итоге будет меньше циклов
				if (IsPlayerValid(j) && GetClientTeam(j) == 2)
					if (GetVectorDistance(fPos[i], fPos[j]) <= 100.0 ) {
						bNear[i][j] = true;
						bNear[j][i] = true;
						//PrintToConsole(client, "%N(%i) рядом с %N(%i) ", i, i, j, j);
					}

	//PrintToConsole(client, "___________________________Тройной ЦИКЛ!!!!!!!!!!!!!!!!!!!!!");
	for (int i=MaxClients; i>0; i--)
		if (IsPlayerValid(i) && GetClientTeam(i) == 2)
			for (int j=i-1; j>0; j--)
				if (IsPlayerValid(j) && GetClientTeam(j) == 2)
					if (bNear[i][j] == true)
						for (int z=MaxClients; z>0; z--)
							if (IsPlayerValid(z) && GetClientTeam(z) == 2 && i != z && j != z)
								if (bNear[i][z] == true) {
									bNear[j][z] = true;
									bNear[z][j] = true;
									//PrintToConsole(client, "%N(%i) рядом с %N(%i) JJJ", j, j, z, z);
								} else if (bNear[j][z] == true) {
									bNear[i][z] = true;
									bNear[z][i] = true;
									//PrintToConsole(client, "%N(%i) рядом с %N(%i) III", i, i, z, z);
								}
	
	int count;
	int with_me[MAXPLAYERS+1];
	for (int i=1; i<=MaxClients; i++)
		if (IsPlayerValid(i) && GetClientTeam(i) == 2) {
			for (int j=1; j<=MaxClients; j++)
				if (IsPlayerValid(j) && GetClientTeam(j) == 2 && i !=j)
					if (bNear[i][j] == true) {
						with_me[i]++;
						//PrintToConsole(client, "%N(%i) в одной толпе с %N(%i) ", i, i, j, j);
					} //else PrintToConsole(client, "%N(%i) ННННЕЕЕЕЕЕЕ в одной толпе с %N(%i) ", i, i, j, j);
			//PrintToConsole(client, "%N в одной толпе с %i зеками", i, with_me[i]+1);
			if (count < with_me[i]+1)
				count = with_me[i]+1;
			if (count == iAllts) {			// если все здесь
				g_bGames = true;
				if (client) {
					if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != count) {
						g_fFloodTime = GetGameTime();
						g_iCount = count;
						CGOPrintToChatAll("Все {BLUE}%d{DEFAULT} заключенных на месте.", iAllts);
					} else PrintToChat(client, "Все %d заключенных на месте.", iAllts);
				} else PrintToServer("Все %d заключенных на месте.", iAllts);
				//dbgMsg("Все %d заключенных на месте.", iAllts);
				Call_StartForward(g_everyone_is_here_every);
				Call_Finish();
				g_bTpos = false;
				if (!g_bAll_t) {
					Call_StartForward(g_everyone_is_here);
					Call_Finish();
					g_bAll_t = true;
				}
				return Plugin_Continue;
			}
		}

	if (iAllts == 0) {
		PrintToChatAll("Нет заключенных");
		//dbgMsg("Нет заключенных");
	} else  if (count > 2) {
		Call_StartForward(g_countts_check);
		Call_Finish();
		float fFlood = g_fFloodTime;
		if (client) {
			if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != count) {
				g_fFloodTime = GetGameTime();
				g_iCount = count;
				CGOPrintToChatAll("На месте {BLUE}%d/%d{DEFAULT}, отсутствуют {BLUE}%d", count , iAllts, iAllts-count);
			} else PrintToChat(client, "На месте %d/%d, отсутствуют %d ", count , iAllts, iAllts-count);
			PrintToChat(client, "Чтобы найти %d сбежавших используй !tpos", iAllts-count);
		} else PrintToServer("На месте %d/%d, отсутствуют %d ", count , iAllts, iAllts-count);
		bool bSay = (count && iAllts/count > 2.4 && GetGameTime()-fFlood > 20.0)? true:false;
		g_bTpos = true;
		KillTimerTPos();
		//dbgMsg("На месте %d/%d, отсутствуют %d ", count , iAllts, iAllts-count);
		for (int i=1; i<=MaxClients; i++) {
			if (IsPlayerValid(i) && GetClientTeam(i) == 2) {
				if (with_me[i]+1 < count) {
					g_bRunner[i] = true;
					if (bSay)
						PrintToChat(i, "Если вы хотите сдаться, то напишите !cap, после чего КТ сможет вас телепортировать к себе, при этом вас не убьют за прошлые бунты");
					//PrintToChatAll("%N сбежал ", i );
				} else {
					g_bRunner[i] = false;
					DPos(i);
				}
			}
		}
	} else {
		if (client) {
			if (GetGameTime() - g_fFloodTime > 5.0 || g_iCount != count) {
				g_fFloodTime = GetGameTime();
				g_iCount = count;
				CGOPrintToChatAll("Все {BLUE}%d{DEFAULT} отсутствуют", iAllts);
			} else PrintToChat(client, "Все %i отсутствуют", iAllts);
			PrintToChat(client, "Чтобы найти %d сбежавших используй !tpos", iAllts);
		} else PrintToServer("Все %i отсутствуют", iAllts);
		g_bTpos = true;
		KillTimerTPos();
		//dbgMsg("Все %i отсутствуют", iAllts);
		for (int i=1; i<=MaxClients; i++)
			if (IsPlayerValid(i) && GetClientTeam(i) == 2)
				g_bRunner[i] = true;
	}
	return Plugin_Continue;
}

stock bool IsPlayerValid(int client)
{
	if (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client))// || IsFakeClient(client) || IsPlayerGhost(client)
	//if (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client) ||  IsFakeClient(client))
		return false;
	return true;
}