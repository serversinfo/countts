#if defined _countts_included
  #endinput
#endif
#define _countts_included


/*********************************************************
 * если игрока не было в строю
 *
 * @param client		The client to run the check on
 * @true on match, false if not
 *********************************************************/
native void IsTRunner(int client);

/*********************************************************
 * Дуступно ли написание !tpos 
 *
 * @true on match, false if not
 *********************************************************/
native void TPosAvailable();

/*********************************************************
 * Дуступны ли игры 
 *
 * @true on match, false if not
 *********************************************************/
native void GamesAvailable();

// everyone_is_here когда все собраны